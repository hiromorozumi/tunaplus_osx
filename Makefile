EXEFILEDEV = tunaplus_dev.exe
EXEFILE = tunaplus.exe
CPPFILES = GUI.cpp Audio.cpp Pitch.cpp Tap.cpp OSC.cpp Metronome.cpp tunerMain.cpp
DSPSOURCE = ./lib/DspFilters/Param.cpp ./lib/DspFilters/Filter.cpp ./lib/DspFilters/Biquad.cpp ./lib/DspFilters/RBJ.cpp
ICONRES = tunaplus.res

INCLUDEPATHS = -I./include
LIBPATHS = -L./lib ./lib/portaudio_x86.lib
LIBFLAGS = -lfftw3 -lsfml-graphics -lsfml-window -lsfml-system

MACEXEDEV = tunaplus_dev
MACEXE = tunaplus
MACEXEDBG = tunaplus_d
MACINCLUDEPATHS = -I./include -I/usr/local/include
MACLIBPATHs = -L/usr/local/lib
MACLIBFLAGS = -lfftw3 -lportaudio -lsfml-graphics -lsfml-window -lsfml-system

normal:
	g++ $(ICONRES) $(CPPFILES) $(DSPSOURCE) $(INCLUDEPATHS) $(LIBPATHS) $(LIBFLAGS) -o $(EXEFILEDEV)
	
release:
	g++ $(ICONRES) $(CPPFILES) $(DSPSOURCE) $(INCLUDEPATHS) $(LIBPATHS) $(LIBFLAGS) -o $(EXEFILE) -mwindows	

debug:
	g++ -g $(ICONRES) $(CPPFILES) $(DSPSOURCE) $(INCLUDEPATHS) $(LIBPATHS) $(LIBFLAGS) -o $(EXEFILEDEV)

mac:
	clang++ -stdlib=libc++ $(CPPFILES) $(DSPSOURCE) $(MACINCLUDEPATHS) $(MACLIBPATHS) $(MACLIBFLAGS) -o $(MACEXE)

macdebug:
	clang++ -stdlib=libc++ -g $(CPPFILES) $(DSPSOURCE) $(MACINCLUDEPATHS) $(MACLIBPATHS) $(MACLIBFLAGS) -o $(MACEXEDBG)
	
clean:
	rm $(EXEFILEDEV)
	rm $(EXEFILE)