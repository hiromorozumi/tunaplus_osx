#!/bin/bash

#
#	create the necessary folder structure
#

if [ -d "./release" ];then
	rm -r ./release
fi

mkdir -p ./release
mkdir -p ./release/TunaPlus.app
mkdir -p ./release/TunaPlus.app/Contents
mkdir -p ./release/TunaPlus.app/Contents/Resources
mkdir -p ./release/TunaPlus.app/Contents/libs
mkdir -p ./release/TunaPlus.app/Contents/MacOS
mkdir -p ./release/TunaPlus.app/Contents/MacOS/res
mkdir -p ./release/TunaPlus.app/Contents/MacOS/res/icon
mkdir -p ./release/TunaPlus.app/Contents/MacOS/documentation

#
#	create the "Info.plist" file
#

cp ./Info.plist ./release/TunaPlus.app/Contents/.

#
#	create the icon set then copy them
#

ICONSIN=./res/icon
ICONTEMP=tunaplus.iconset
ICONSOUT=./release/TunaPlus.app/Contents/Resources

mkdir -p "$ICONTEMP"
sips -z 16 16	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_16x16.png
sips -z 32 32	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_16x16@2x.png
sips -z 32 32	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_32x32.png
sips -z 64 64	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_32x32@2x.png
sips -z 128 128	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_128x128.png
sips -z 256 256	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_128x128@2x.png
sips -z 256 256	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_256x256.png
sips -z 512 512	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_256x256@2x.png
sips -z 512 512	$ICONSIN/tunaplus_icon_1024.png --out $ICONTEMP/icon_512x512.png
cp $ICONSIN/tunaplus_icon_1024.png $ICONTEMP/icon_512x512@2x.png
iconutil --convert icns tunaplus.iconset
rm -R ./tunaplus.iconset
cp ./tunaplus.icns $ICONSOUT/.

#
#	copy all the dylibs that executable depends on
#

# DYLIBDEST=./release/TunaPlus.app/Contents/MacOS/libs

# cp /usr/local/opt/fftw/lib/libfftw3.3.dylib $DYLIBDEST
# cp /usr/local/opt/portaudio/lib/libportaudio.2.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-graphics.2.4.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-window.2.4.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-system.2.4.dylib $DYLIBDEST
# cp /usr/lib/libc++.1.dylib $DYLIBDEST
# cp /usr/lib/libSystem.B.dylib $DYLIBDEST

#
#	update the dylib paths for the bundled executable to refer to
#

# install_name_tool -change /usr/local/opt/fftw/lib/libfftw3.3.dylib @executable_path/libfftw3.3.dylib tunaplus
# install_name_tool -change /usr/local/opt/portaudio/lib/libportaudio.2.dylib @executable_path/libportaudio.2.dylib tunaplus
# install_name_tool -change @rpath/libsfml-graphics.2.4.dylib @executable_path/libsfml-graphics.2.4.dylib tunaplus
# install_name_tool -change @rpath/libsfml-window.2.4.dylib @executable_path/libsfml-window.2.4.dylib tunaplus
# install_name_tool -change @rpath/libsfml-system.2.4.dylib @executable_path/libsfml-system.2.4.dylib tunaplus
# install_name_tool -change /usr/lib/libc++.1.dylib @executable_path/libc++.1.dylib tunaplus
# install_name_tool -change /usr/lib/libSystem.B.dylib @executable_path/libSystem.B.dylib tunaplus

#
#	copy the executable into the Contents/MacOS folder
#

cp ./tunaplus ./release/TunaPlus.app/Contents/MacOS/.

#
#	bundle all dependent dylibs using macdylibbundler
#

dylibbundler -od -b -x ./release/Tunaplus.app/Contents/MacOS/tunaplus -d ./release/Tunaplus.app/Contents/libs

#
#	copy all additional files that the executable uses into Contents/MacOS
#

cp ./res/* ./release/TunaPlus.app/Contents/MacOS/res/
cp ./res/icon/* ./release/TunaPlus.app/Contents/MacOS/res/icon/
cp ./documentation/* ./release/TunaPlus.app/Contents/MacOS/documentation/
cp ./LICENSE.txt ./release/TunaPlus.app/Contents/MacOS/
cp ./README.txt ./release/TunaPlus.app/Contents/MacOS/

#
#	create a zip file of the application...
#

cd ./release
zip -r "TunaPlus.zip" ./TunaPlus.app
mv ./TunaPlus.zip ./TunaPlus_v0-1-0_mac.zip

echo "FINISHED! Now you have the app bundle and a zip file of the app bundle in the 'release' folder. Opening the 'release' folder now..."

open ./
