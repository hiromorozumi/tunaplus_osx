#ifndef TAP_H
#define TAP_H

#ifdef _WIN32

	#include <windows.h>

#elif __APPLE__

	//
	//	TODO
	//
	
#endif
	
#include <SFML/System.hpp>
#include <iostream>
#include <deque>

class Tap
{
public:

	sf::Clock tapClock;

	std::deque<double> noteLength;
	unsigned long long startedTime;
	long elapsedTime;
	double currentAverageLength;
	double currentBPM;

	Tap();
	~Tap(){}
	
	void tap();
	bool isBPMReady();
	double getBPM();
	void setBPM(double bpm);
	
	unsigned long long getTimeMSx100();
	void restartTimer();
	long getElapsedTime();
	void pushHistory(double value);
	int getHistorySize();
	void clearHistory();
	double getAverage();
	
protected:

};

#endif