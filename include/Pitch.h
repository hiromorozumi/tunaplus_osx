// Pitch class /////////////////////////////////////////////////////////////////////

#ifndef PITCH_H
#define PITCH_H

#ifdef _WIN32
	#include <Windows.h>
	#include <fftw/fftw3.h>
#elif __APPLE__
	#include <fftw3.h>
#endif

#include <vector>
#include <deque>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <DspFilters/Dsp.h>
#include "Audio.h"

class Pitch
{
public:

	static const double BASE_THRESHOLD_AMPLITUDE;
	static const double TWOPI;
	static const int N;
	static const double sampleRate;
	static const int HISTORY_SIZE;
	static const int IIR_BUFFER_SIZE;

	fftw_complex *in, *out;
	fftw_plan p;
	Audio* audio;
	double magnitude[65535];
	double audioInBoostFactor;
	
	Dsp::Filter *filter;
    Dsp::Params dspParams;
	double* filterBuffer;
	double iirBuffer[2048];
	
	double frequencyTable[108];
	double semitoneRatio;
	double referencePitch;
	double middleC;
	double cZero;
	
	double currentFrequency;
	int currentNoteNumber;
	double currentNoteNumberDouble;
	std::string currentNoteName;
	double currentNoteCenterFrequency;
	double currentFreqencyDelta;
	double currentNoteDeltaPercent;
	double currentPeakAmplitude;
	double thresholdAmplitude;

	std::deque<double> frequencyHistory;
	std::deque<int> deltaPercentHistory;
	
	double fundamentalFreqAtHalf;
	double fundamentalFreqAtOneThird;
	double fundamentalFreqAtOneFourth;
	bool fundamentalFoundAtHalf;
	bool fundamentalFoundAtOneThird;
	bool fundamentalFoundAtOneFourth;
	
	Pitch();
	~Pitch(){}
	void initialize();
	void refresh();
	void bindAudio(Audio* r);
	void setAudioInBoostFactor(double factor);
	double getAudioInBoostFactor();
	double detect();
	double tau(double x);
	void buildFrequencyTable(double refPitch);
	double noteToFrequency(int noteNum);
	double frequencyToNote(double freq);
	double getCurrentNoteDeltaPercent();
	std::string noteToString(int noteNum);
	bool loudEnough();
	std::string getCurrentNoteName();
	void pushHistory();
	double getFrequencyAvg();
	double getDeltaPercentAvg();
};

#endif